from flask import Flask, render_template, redirect, url_for, session,request
import json
import matplotlib.pyplot as plt
import io
import base64

class Data():
    data = []
    
    def __init__(self, path):
        self.path = path
        self.data = self.load_data()
        
        
    def load_data(self):
        try:
            with open(self.path, "r") as fp:
                return json.load(fp)
        except FileNotFoundError:
            return []
        
    
            
    def list(self):
        for a in self.data:
            print(a)


d =Data("data.json")
d.list()        

app = Flask(__name__)
app.secret_key = 'your_secret_key'




def create_temperature_plot(timestamps, temperatures):
    plt.figure(figsize=(8, 6))
    plt.plot(timestamps, temperatures)
    plt.xlabel('Cas')
    plt.ylabel('Teplota')
    plt.title('Vysledek')
    plt.grid(True)
    
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    
    plot_base64 = base64.b64encode(buffer.read()).decode('utf-8')
    buffer.close()
    
    return plot_base64


@app.context_processor
def inject_current_user():
    return dict(current_user=session.get('current_user'))

@app.route("/")
def home():
    return render_template('base.html')


@app.route("/login", methods = ['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']

        session['current_user'] = username

        return redirect(url_for('dashboard'))

    return render_template('login.html')


@app.route("/register")
def register():
        return render_template('register.html')
    
    
@app.route('/dashboard')
def dashboard():
    num_entries = int(request.args.get('num_entries', 6))
    reversed_data = list(reversed(d.data))[:num_entries]
    
    timestamps = [entry['timestamp'] for entry in reversed_data]
    temperatures = [entry['temp'] for entry in reversed_data]

    plot_base64 = create_temperature_plot(timestamps, temperatures)
    return render_template('dashboard.html', data=reversed_data, plot_base64=plot_base64)
    



@app.route('/delete_entries', methods=['POST'])
def delete_entries():
    num_to_delete = int(request.form['num_to_delete'])  
    del d.data[:num_to_delete]  
    return redirect(url_for('dashboard'))  

if __name__ =="__main__":
    app.run(debug = True) 