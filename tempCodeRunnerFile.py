def create_temperature_plot(timestamps, temperatures):
    plt.figure(figsize=(8, 6))
    plt.plot(timestamps, temperatures)
    plt.xlabel('Cas')
    plt.ylabel('Teplota')
    plt.title('Vysledek')
    plt.grid(True)
    plt.savefig('temperature_plot.png')  # Сохраняем график как изображение
    plt.close()